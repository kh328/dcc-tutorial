#!/bin/bash
#
#SBATCH --job-name=myjobRmc
#SBATCH --mail-user=someone@somewhere.edu
#SBATCH --mail-type=END,FAIL
#SBATCH --time=03:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH --mem-per-cpu=1G
#SBATCH --output=slurm-R-mc-job-%J.stdout
#SBATCH --error=slurm-R-mc-job-%J.stderr

module load R/3.4.4

R CMD BATCH mytestR-mc-2.R

module unload R/3.4.4
