#!/bin/bash
#
#SBATCH --job-name=myjobcpp
#SBATCH --mail-user=
#SBATCH --mail-type=END,FAIL
#SBATCH --mem=1024
#SBATCH --time=03:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --output=slurm-cpp-job-%J.stdout
#SBATCH --error=slurm-cpp-job-%J.stderr

g++ -v
g++ -O2 -o mytestcpp.out mytestcpp.cpp
./mytestcpp.out
