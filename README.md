# Notes
* This repository contains instructions for B&B students using the Duke Compute Cluster (DCC) resources
* It also provides a number of sample scripts for batch processing
* Do NOT under any circumstances store PHI data on the DCC. 
* Be sure to review Tom Milledge's excellent tutorial ( https://rc.duke.edu/wp-content/uploads/DCC_Workshop_04-13-2017.pdf). The material shown here is mostly taken from these slides.
* This tutorial is meant to help you get started. Read Tom's slides if you decide to 

# Utilities
* SCM: hg (/opt/apps/Python-2.7.10/bin/hg), git
* editors:   emacs, nano, vi
* File transfer: wget, rsync, sftp, scp
* compilers: gcc, gfortran, javac
* programming languages : python, ruby, scala

# Nodes available to B&B faculty, staff and students

## Priority nodes for B&B faculty, staff and students

### CPU nodes: 
* dcc-biostat-01
* dcc-biostat-02
* dcc-biostat-03

## Common CPU nodes 

* common (up to 64 GB)
* Common-large (64GB to 240GB)

## Common GPU nodes

* gpu-common


# Log into the DCC (from a terminal)

## Login and Data Transfer Nodes:
* dcc-slogin-01.oit.duke.edu
* dcc-slogin-02.oit.duke.edu

Note: If you are outside the Duke firewall, you need to use MFA to authenticate 
(after submitting your password, you will be prompted to enter an SMS passcode
or to MFA through phone or Duo). Alternatively, you can ssh through VPN to skip
the MFA.

## ssh from shell
$ ssh NETID@dcc-slogin-01.oit.duke.edu
## Upload file to machine
$ scp foo.txt NETID@dcc-slogin-02.oit.duke.edu: 
## Upload directory
$ scp -r mylocaldir/ NETID@dcc-slogin-02.oit.duke.edu: 
## Download file to local machine
$ scp NETID@dcc-slogin-02.oit.duke.edu:foo.txt . 
## Download directory to local machine
$ scp -r NETID@dcc-slogin-02.oit.duke.edu:foo.txt:remotedir/ . 


# Maneuver DCC: Submit Batch Job

Note: The myscriptR.q referenced in this section can be found in
the repository

## Submit a batch job (let job scheduler find a node for you)
$ sbatch myscriptR.q

## Submit a batch job to a specific machine

$ sbatch -p biostat -w dcc-biostat-01 myscriptR.q

## Submit a batch job to the common partition (up to 64GB)
$ sbatch -p common myscriptR.q

## Submit a batch job to the large common partition (up to 64-240 GB)
$ sbatch -p common-large myscriptR.q


# Maneuver DCC: Interactive Computing from shell

Note: You may not run computing jobs from the login nodes.
Use an interactive shell instead.
## To run interactive shell on CPU node
$ srun --pty bash -i
## To run interactive shell on CPU node allocate 4GB of RAM
$ srun --mem=4G --pty bash -i
## To run interactive shell on CPU node on a specific  B&B node
$ srun -p biostat -w dcc-biostat-01 -A biostat --pty bash -i

# Maneuver DCC: More customized examples

## Submit batch job to a specific node
$ sbatch  -p owzarteam -A owzarteam -w dcc-owzarteam-01 myscriptR.q
## On the GPU nodes (note --gres=gpu)
$ sbatch -p owzarteam -w dcc-owzarteam-gpu-01 --gres=gpu myscript-CUDA.q
## Submit batch job to the biostat node
## Note that the --account has to be set if default account is not biostat 
$ sbatch -p biostat -A biostat -w dcc-biostat-01 myscript-cpp.q
## To use multiple partitions
$ sbatch -p biostat,owzarteam --account=biostat -w dcc-biostat-01 myscript-cpp.q 


# Get information about the jobs
## Check the status of your jobs
$ squeue -u NETID
## Get information about a job
$ scontrol show job JOBID
## Cancel a job
$ scancel JOBID
## Get accounting data
$ sacct

# Software Modules

Software packages on the DCC are managed as modules. You need to
load a the relevant module before you can use the software

## List available modules

$ module avail

## Add an available module

$ module load R/3.6.0

## Unload the module

$ module unload R/3.6.0


# Using CUDA
## Check for GPU card (after running interactive shell on GPU node)
$ nvidia-smi -q
## Set CUDA paths (check to make sure that the path exists)
$ export LD_LIBRARY_PATH=/usr/local/cuda/lib64
#
$ export PATH=/usr/local/cuda/bin:$PATH
Run CUDA jobs on the common GPU nodes
## Batch mode
$ sbatch  -p gpu-common --gres=gpu myscript-CUDA.q
## Interactive mode
$ srun -p gpu-common  --gres=gpu --pty bash -i

# Administrative
## List users in a group
$ sacctmgr list associations Account=owzarteam
#
$ sacctmgr list associations Account=biostat

## Check access for user
$ sacctmgr list associations User=NETID

## Get information about a node
$ scontrol show node dcc-biostat-02


# Storage (may need to be revised soon)

## Home directory

Your home directory on the DCC cluster is /dscrhome/NETID . These directories are backed up but are subject to quotas. You should use this space for storing your code (preferrably using SCM) and smaller data files

## Work directory

You can create a folder under /work to temporarily save large files. These files will not be backed up and will automatically deleted depending on age of the files and available capacity.

## Archiving

You can create a folder under /datacommons/biostat for archiving important files. These files are currently NOT backed up. 
